import os

from app import app
from flask import render_template


def get_articles():
    path = 'app/articles'
    articles = []
    for article_name in os.listdir(path):
        article = {}
        with open(os.path.join(path, article_name), 'r') as f:
            article['title'] = f.readline()
            article['content'] = f.read()
        articles.append(article)
    return articles


@app.route('/')
@app.route('/index')
def index():
    _articles = get_articles()
    return render_template('articles.html', articles=_articles)

