from flask import Flask

app = Flask(__name__)

app.config['CSRF_ENABLED'] = True
app.config['SECRET_KEY'] = 'test-secret-key'

from app import routes

