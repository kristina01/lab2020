import os

from app import app
from flask import render_template, flash, redirect, request, url_for
from .forms import LoginForm, RegistrationForm


def get_articles():
    path = 'app/articles'
    articles = []
    for article_name in os.listdir(path):
        article = {}
        with open(os.path.join(path, article_name), 'r') as f:
            article['title'] = f.readline()
            article['content'] = f.read()
        articles.append(article)
    return articles


@app.route('/')
@app.route('/index')
def index():
    _articles = get_articles()
    return render_template('articles.html', articles=_articles)


@app.route('/sign_in', methods=['GET', 'POST'])
def sign_in():
    login = 'user'
    password = 'password123'

    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        if form.login.data == login and form.password.data == password:
            flash('Вход успешно выполнен!', 'sign_in')
            return redirect(url_for('index'))
        else:
            flash('Неверный логи или пароль', 'sign_in_error')
            return render_template('sign_in.html', form=form)
    return render_template('sign_in.html', form=form)


@app.route('/sign_up', methods=['GET', 'POST'])
def sign_up():
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        if form.password.data == form.second_password.data:
            flash('Пользователь {} успешно зарегистрирован!'.format(form.login.data), 'sign_up')
            return redirect(url_for('index'))
        else:
            flash('Пароли не совпадают', 'sign_up_error')
            return render_template('sign_up.html', form=form)
    return render_template('sign_up.html', form=form)

