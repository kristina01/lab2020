from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, BooleanField, PasswordField
from wtforms.validators import Required, Length


class LoginForm(FlaskForm):
    login = StringField('User name:', [Required(), Length(max=32)])
    password = PasswordField('Password', [Required(), Length(min=4, max=32)])
    remember_me = BooleanField('Remember me', default = False)
    submit = SubmitField('Sign in')


class RegistrationForm(FlaskForm):
    login = StringField('User name:', [Required(), Length(max=32)])
    password = PasswordField('Password', [Required(), Length(min=4, max=32)])
    second_password = PasswordField('Repeat password', [Required(), Length(min=4, max=32)])
    submit = SubmitField('Sign up')

