from departament import Departament
from worker import Worker


class Menu:

    def __init__(self, departament: Departament):
        self.departament = departament

    def show(self):
        self._main_menu()

    def _select_one_of(self, container, title: str = ''):
        items = {idx: item for idx, item in enumerate(container, start=1)}
        if title:
            print(title)
        while True:
            for idx in sorted(items.keys()):
                item = items[idx]
                print(str(idx) + ')', item)
            try:
                selected_idx = int(input('Выберите один из варинтов: '))
            except ValueError:
                print('Ошибка ввода')
                continue

            item = items.get(selected_idx, None)
            print(item)
            if item is not None:
                return selected_idx, item
            else:
                print('Такого варианта не существует')
                continue

    def _main_menu(self):

        menu = {
                1: ('Добавить сотрудника', self.create_worker),
                2: ('Трудоустроить сотрудника', self.employ),
                3: ('Удалить сотрудника', self.remove_worker),
                4: ('Уволить сотрудника', self.dismiss_worker),
                5: ('Показать информацию о сотруднике', self.worker_info),
                6: ('Показать информацию об отеделе', self.departament_info),
                7: ('Загрузить информацию об отделе', self.load),
                8: ('Сохранить информацию об отделе', self.save),
                9: ('Выход', self.exit),
        }

        menu_items = [menu[idx][0] for idx in sorted(menu.keys())]

        result = 1
        while result != -1:
            choosen_idx, _ = self._select_one_of(menu_items,)
            _, func = menu[choosen_idx]
            result = func()
            print()

    def create_worker(self):
        name, second_name = input('Введите Имя Фамилию нового сотрудника: ').split()
        worker = Worker(name, second_name)
        self.departament.add_worker(worker)
        print('Сотрудник создан!')

    def employ(self):
        workers = self.departament.get_workers()
        worker_names = [worker.full_name() for worker in workers]
        idx, _ = self._select_one_of(worker_names, 'Выберите сотрудника: ')
        worker = workers[idx - 1]

        positions = self.departament.get_positions()
        _, position_name = self._select_one_of(positions.keys(), 'Выберите должность: ')

        max_rate = positions[position_name]
        rate = float(input('Введите ставку ({} - максимальная доступная ставка): '.format(max_rate)))
        position_rate = min(max_rate, rate)

        self.departament.employ(worker, position_name, position_rate)
        print('Сотрудник трудоустроен!')

    def remove_worker(self):
        workers = self.departament.get_workers()
        worker_names = [worker.full_name() for worker in workers]
        idx, _ = self._select_one_of(worker_names, 'Выберите сотрудника, которого хотите удлаить: ')
        self.departament.remove_worker(workers[idx - 1])
        print('Сотрудник удален!')

    def dismiss_worker(self):
        workers = self.departament.get_workers()
        worker_names = [worker.full_name() for worker in workers]
        idx, _ = self._select_one_of(worker_names, 'Выберите сотрудника, которого хотите уволить: ')
        self.departament.dismiss(workers[idx - 1])
        print('Сотрудник уволен!')

    def worker_info(self):
        workers = self.departament.get_workers()
        worker_names = [worker.full_name() for worker in workers]
        idx, _ = self._select_one_of(worker_names, 'Выберите сотрудника: ')
        print(workers[idx - 1].info())

    def departament_info(self):
        print(self.departament.info())

    def save(self):
        path = input('Введите имя файла для сохранения: ')
        self.departament.save(path)

    def load(self):
        path = input('Введите имя файла для загрузки: ')
        self.departament.load(path)

    def exit(self):
        return -1


def main():
    departament = Departament('IT отдел')
    menu = Menu(departament)
    menu.show()


if __name__ == '__main__':
    main()

