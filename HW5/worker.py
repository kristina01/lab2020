class Worker:

    def __init__(self, name: str, second_name: str):
        self.name = name
        self.second_name = second_name
        self.positions = {}
        self.departament = None

    def full_name(self):
        return '{} {}'.format(self.name, self.second_name)

    def get_positions(self):
        return self.positions

    def set_position(self, position_name: str, position_rate: float):
        self.positions[position_name] = self.positions.get(position_name, 0) + position_rate

    def set_departament(self, departament: str):
        self.departament = departament

    def clear_positions(self):
        self.positions = {}

    def info(self) -> str:
        information = ['Сотрудник {} {}'.format(self.name, self.second_name)]
        for position_name, position_rate in self.positions.items():
            information.append('{}: %{}'.format(position_name, position_rate))
        return '\n'.join(information)

