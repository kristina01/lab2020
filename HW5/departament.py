from worker import Worker


class Departament:

    def __init__(self, name: str):
        self.name = name
        self.workers = []
        self.positions = {
        }

    def get_workers(self):
        return self.workers

    def get_positions(self):
        return self.positions

    def add_position(self, position_name: str, position_rate: float):
        self.positions[position_name] = position_rate

    def add_worker(self, worker: Worker):
        self.workers.append(worker)

    def employ(self, worker: Worker, position_name: str, position_rate: float):
        self.positions[position_name] -= position_rate
        worker.set_position(position_name, position_rate)
        worker.set_departament(self.name)

    def dismiss(self, worker: Worker):
        for position_name, position_rate in worker.get_positions().items():
            self.positions[position_name] += position_rate
        worker.clear_positions()

    def remove_worker(self, worker: Worker):
        self.dismiss(worker)
        self.workers.remove(worker)

    def info(self) -> str:
        information = ['{}:'.format(self.name)]
        for position_name, position_rate in self.positions.items():
            information.append('{}: {} % ставок'.format(position_name, position_rate))

        for worker in self.workers:
            information.append(worker.info())

        return '\n'.join(information)

    def save(self, path: str):
        with open(path, 'w') as f:
            f.write(self.info())

    def load(self, path: str):
        self.workers = []
        self.positions = {}

        with open(path, 'r') as f:
            lines = f.readlines()
        if lines:
            self.name = lines[0][:-1]
            print(self.name)

            idx = 1
            while idx < len(lines) and not lines[idx].startswith('Сотрудник'):
                position_name, position_rate = lines[idx].split(':')
                position_rate = position_rate.split('%')[0]
                self.positions[position_name] = float(position_rate)
                idx += 1

            while idx < len(lines) and lines[idx].startswith('Сотрудник'):
                name, second_name = lines[idx].replace('Сотрудник ', '').split()
                worker = Worker(name, second_name)
                self.add_worker(worker)
                idx += 1
                while idx < len(lines) and not lines[idx].startswith('Сотрудник'):
                    position_name, position_rate = lines[idx].split(':')
                    position_rate = position_rate.split('%')[0]
                    self.employ(worker, position_name, float(position_rate))
                    idx += 1

